use scootaloo::parse_toml;
use std::collections::HashMap;

#[test]
fn test_alt_services() {
    let toml = parse_toml("tests/no_test_alt_services.toml");
    assert_eq!(toml.scootaloo.alternative_services_for, None);

    let toml = parse_toml("tests/test_alt_services.toml");
    assert_eq!(
        toml.scootaloo.alternative_services_for,
        Some(HashMap::from([
            ("tamere.lol".to_string(), "tonpere.mdr".to_string()),
            ("you.pi".to_string(), "you.pla".to_string())
        ]))
    );
}

#[test]
fn test_re_display() {
    let toml = parse_toml("tests/no_show_url_as_display_url_for.toml");
    assert_eq!(toml.scootaloo.show_url_as_display_url_for, None);

    let toml = parse_toml("tests/show_url_as_display_url_for.toml");

    assert_eq!(
        toml.scootaloo.show_url_as_display_url_for,
        Some("^(.+)\\.es$".to_string())
    );
}

#[test]
fn test_page_size() {
    const DEFAULT_PAGE_SIZE: i32 = 200;
    let toml = parse_toml("tests/page_size.toml");

    assert_eq!(toml.twitter.page_size, Some(100));

    assert_eq!(toml.mastodon.get("0").unwrap().twitter_page_size, None);

    assert_eq!(toml.mastodon.get("1").unwrap().twitter_page_size, Some(42));

    // this is the exact line that is used inside fn run() to determine the twitter page size
    // passed to fn get_user_timeline()
    let page_size_for_0 = toml
        .mastodon
        .get("0")
        .unwrap()
        .twitter_page_size
        .unwrap_or_else(|| toml.twitter.page_size.unwrap_or(DEFAULT_PAGE_SIZE));
    let page_size_for_1 = toml
        .mastodon
        .get("1")
        .unwrap()
        .twitter_page_size
        .unwrap_or_else(|| toml.twitter.page_size.unwrap_or(DEFAULT_PAGE_SIZE));

    assert_eq!(page_size_for_0, 100);
    assert_eq!(page_size_for_1, 42);

    let toml = parse_toml("tests/no_page_size.toml");

    assert_eq!(toml.twitter.page_size, None);
    assert_eq!(toml.mastodon.get("0").unwrap().twitter_page_size, None);

    // and same here
    let page_size_for_0 = toml
        .mastodon
        .get("0")
        .unwrap()
        .twitter_page_size
        .unwrap_or_else(|| toml.twitter.page_size.unwrap_or(DEFAULT_PAGE_SIZE));

    assert_eq!(page_size_for_0, 200);
}

#[test]
fn test_parse_good_toml_rate_limit() {
    let parse_good_toml = parse_toml("tests/good_test_rate_limit.toml");

    assert_eq!(parse_good_toml.scootaloo.rate_limit, Some(69 as usize));
}

#[test]
fn test_parse_good_toml_mastodon_screen_name() {
    let parse_good_toml = parse_toml("tests/good_test_mastodon_screen_name.toml");

    assert_eq!(
        parse_good_toml
            .mastodon
            .get("0")
            .unwrap()
            .mastodon_screen_name,
        Some("tarace".to_string())
    );

    assert_eq!(
        parse_good_toml
            .mastodon
            .get("1")
            .unwrap()
            .mastodon_screen_name,
        None
    );
}

#[test]
fn test_parse_good_toml() {
    let parse_good_toml = parse_toml("tests/good_test.toml");

    assert_eq!(
        parse_good_toml.scootaloo.db_path,
        "/var/random/scootaloo.sqlite"
    );
    assert_eq!(parse_good_toml.scootaloo.cache_path, "/tmp/scootaloo");
    assert_eq!(parse_good_toml.scootaloo.rate_limit, None);

    assert_eq!(parse_good_toml.twitter.consumer_key, "rand consumer key");
    assert_eq!(parse_good_toml.twitter.consumer_secret, "secret");
    assert_eq!(parse_good_toml.twitter.access_key, "rand access key");
    assert_eq!(parse_good_toml.twitter.access_secret, "super secret");

    assert_eq!(
        &parse_good_toml
            .mastodon
            .get("tamerelol")
            .unwrap()
            .twitter_screen_name,
        "tamerelol"
    );
    assert_eq!(
        &parse_good_toml.mastodon.get("tamerelol").unwrap().base,
        "https://m.nintendojo.fr"
    );
    assert_eq!(
        &parse_good_toml.mastodon.get("tamerelol").unwrap().client_id,
        "rand client id"
    );
    assert_eq!(
        &parse_good_toml
            .mastodon
            .get("tamerelol")
            .unwrap()
            .client_secret,
        "secret"
    );
    assert_eq!(
        &parse_good_toml.mastodon.get("tamerelol").unwrap().redirect,
        "urn:ietf:wg:oauth:2.0:oob"
    );
    assert_eq!(
        &parse_good_toml.mastodon.get("tamerelol").unwrap().token,
        "super secret"
    );
}

#[test]
#[should_panic(
    expected = "Cannot open config file tests/no_file.toml: No such file or directory (os error 2)"
)]
fn test_parse_no_toml() {
    let _parse_no_toml = parse_toml("tests/no_file.toml");
}

#[test]
#[should_panic(
    expected = "Cannot parse TOML file tests/bad_test.toml: expected an equals, found a newline at line 1 column 5"
)]
fn test_parse_bad_toml() {
    let _parse_bad_toml = parse_toml("tests/bad_test.toml");
}
