**Due to the new Twitter policy about API v1.1 and API v2, this project will no longer be updated as it no longer can work for free any way, shape or form.**

First of all, I’m deeply sorry about that. It worked pretty great for what it did with a level of quality I’m very proud to have achieved.

Secondly, fuck you Musk, fuck you.

A Twitter to Mastodon copy bot written in Rust

It:
* copies the content (text) of the original Tweet
* dereferences the links
* gets every attached media (photo, video or gif)

If any of the last steps failed, the Toot gets published with the exact same text as the Tweet.

RT are excluded, replies are included when considered part of a thread (reply to self), not the actual replies to other Twitter users.

# Usage
## Configuring

First up, create a configuration file (default path is `/usr/local/etc/scootaloo.toml`). It will look like this:

```toml
[scootaloo]
db_path = "/var/lib/scootaloo/scootaloo.sqlite" ## file containing the SQLite Tweet corresponding Toot DB, must be writeable
cache_path = "/tmp/scootaloo" ## a dir where the temporary files will be download, must be writeable
rate_limiting = 4 ## optional, default 4, number of accounts handled simultaneously
## optional, this should be omitted the majority of the time
## sometimes, twitter try to use french inclusive writting, but instead of using `·` (median point), they’re using `.`
## this makes twitter interpret it as a URL, which is wrong
## this parameter allows you to catch such URLs and apply the `display_url` (i.e. `tout.es`) instead of the `expanded_url` (i.e. `http://tout.es`)
## in those particular cases
## (!) use with caution, it might have some undesired effects
show_url_as_display_url_for = "^https?://(.+)\\.es$"
## optional, this allows you to replace the host for popular services such as YouTube of Twitter, or any other
## with their more freely accessible equivalent
[scootaloo.alternative_services_for]
"tamere.lol" = "tonpere.mdr" ## quotes are necessary for both parameters
"you.pi" = "you.pla"
"www.you.pi" = "you.pla" ## this is an exact match, so you’ll need to lay out all the possibilities

[twitter]
## Consumer/Access key for Twitter (can be generated at https://developer.twitter.com/en/apps)
page_size = 20 ## optional, default 200, max number of tweet retrieved
consumer_key = "MYCONSUMERKEY"
consumer_secret = "MYCONSUMERSECRET"
access_key = "MYACCESSKEY"
access_secret = "MYACCESSSECRET"

[mastodon]
```

Then run the command with the `init` subcommand to initiate the DB:
```sh
scootaloo init
```

This subcommand is completely idempotent.

Then run the command with the `register` subcommand:
```sh
scootaloo register --host https://m.nintendojo.fr
```

This will give you the end of the TOML file. It will look like this:

```toml
[mastodon.nintendojofr] ## account
twitter_screen_name = "NintendojoFR" ## User Timeline to copy
mastodon_screen_name = "nintendojofr" ## optional, Mastodon account name used for smart mentions
base = "https://m.nintendojo.fr"
client_id = "MYCLIENTID"
client_secret = "MYCLIENTSECRET"
redirect = "urn:ietf:wg:oauth:2.0:oob"
token = "MYTOKEN"
```

You can add other account if you like, after the `[mastodon]` moniker. Scootaloo would theorically support an unlimited number of accounts.

You can also add a custom twitter page size in this section that would override the global (under the `twitter` moniker) and default one (200), like so:
```
twitter_page_size = 40
```

## Running

You can then run the application via `cron` for example. Here is the generic usage:

```sh
A Twitter to Mastodon bot

Usage: scootaloo [OPTIONS] [COMMAND]

Commands:
  register     Command to register to a Mastodon Instance
  init         Command to init Scootaloo DB
  migrate      Command to migrate Scootaloo DB
  copyprofile  Command to copy a Twitter profile into Mastodon
  help         Print this message or the help of the given subcommand(s)

Options:
  -c, --config <CONFIG_FILE>  TOML config file for scootaloo [default: /usr/local/etc/scootaloo.toml]
  -l, --loglevel <LOGLEVEL>   Log level [possible values: Off, Warn, Error, Info, Debug]
  -h, --help                  Print help information
  -V, --version               Print version information
```

# Quirks

Scootaloo does not respect the spam limits imposed by Mastodon: it will make a 429 error if too much Tweets are converted to Toots in a short amount of time (and it will not recover from it). By default, it gets the last 200 tweets from the user timeline (which is a lot!). It is recommended to put a Tweet number into the DB file before copying an old account.

You can insert that Tweet number, by connecting to the DB you created:
```sh
sqlite3 /var/lib/scootaloo/scootaloo.sqlite
```

And inserting the data:

```sql
INSERT INTO tweet_to_toot VALUES ("<twitter_screen_name>", 1383782580412030982, "<twitter_screen_name>");
```

The last value is supposed to be the Toot ID. It cannot be null, so you better initialize it with something unique, like the Twitter Screen Name for example.

# Migrating from Scootaloo ⩽ 0.6.1

The DB scheme has change between version 0.6.x and 0.7.x (this is due to the multi-account nature of Scootaloo from 0.7.x onward). You need to migrate your DB. You can do so by issuing the command:

```
scootaloo migrate
```

You can optionnally specify a screen name with the `--name` option. By default, it’ll take the first screen name in the config file.
