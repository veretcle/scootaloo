use std::{collections::HashMap, fs::read_to_string};

use serde::Deserialize;

/// General configuration Struct
#[derive(Debug, Deserialize)]
pub struct Config {
    pub twitter: TwitterConfig,
    pub mastodon: HashMap<String, MastodonConfig>,
    pub scootaloo: ScootalooConfig,
}

#[derive(Debug, Deserialize)]
pub struct TwitterConfig {
    pub consumer_key: String,
    pub consumer_secret: String,
    pub access_key: String,
    pub access_secret: String,
    pub page_size: Option<i32>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct MastodonConfig {
    pub twitter_screen_name: String,
    pub mastodon_screen_name: Option<String>,
    pub twitter_page_size: Option<i32>,
    pub base: String,
    pub client_id: String,
    pub client_secret: String,
    pub redirect: String,
    pub token: String,
}

#[derive(Debug, Deserialize)]
pub struct ScootalooConfig {
    pub db_path: String,
    pub cache_path: String,
    pub rate_limit: Option<usize>,
    pub show_url_as_display_url_for: Option<String>,
    pub alternative_services_for: Option<HashMap<String, String>>,
}

/// Parses the TOML file into a Config Struct
pub fn parse_toml(toml_file: &str) -> Config {
    let toml_config = read_to_string(toml_file)
        .unwrap_or_else(|e| panic!("Cannot open config file {toml_file}: {e}"));

    let config: Config = toml::from_str(&toml_config)
        .unwrap_or_else(|e| panic!("Cannot parse TOML file {toml_file}: {e}"));

    config
}
