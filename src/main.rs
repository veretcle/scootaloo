use clap::{Arg, ArgAction, Command};
use log::LevelFilter;
use scootaloo::*;
use simple_logger::SimpleLogger;
use std::str::FromStr;

const DEFAULT_CONFIG_PATH: &str = "/usr/local/etc/scootaloo.toml";

fn main() {
    let matches = Command::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .about("A Twitter to Mastodon bot")
        .arg(
            Arg::new("config")
                .short('c')
                .long("config")
                .value_name("CONFIG_FILE")
                .help("TOML config file for scootaloo")
                .num_args(1)
                .default_value(DEFAULT_CONFIG_PATH)
                .display_order(1),
        )
        .arg(
            Arg::new("log_level")
                .short('l')
                .long("loglevel")
                .value_name("LOGLEVEL")
                .help("Log level")
                .num_args(1)
                .value_parser(["Off", "Warn", "Error", "Info", "Debug"])
                .display_order(2),
        )
        .subcommand(
            Command::new("register")
                .version(env!("CARGO_PKG_VERSION"))
                .about("Command to register to a Mastodon Instance")
                .arg(
                    Arg::new("host")
                        .short('H')
                        .long("host")
                        .value_name("HOST")
                        .help("Base URL of the Mastodon instance to register to (no default)")
                        .num_args(1)
                        .required(true)
                        .display_order(1)
                    )
                .arg(
                    Arg::new("name")
                        .short('n')
                        .long("name")
                        .help("Twitter Screen Name (like https://twitter.com/screen_name, no default)")
                        .num_args(1)
                        .required(true)
                        .display_order(2)
                ),
        )
        .subcommand(
            Command::new("init")
                .version(env!("CARGO_PKG_VERSION"))
                .about("Command to init Scootaloo DB")
                .arg(
                    Arg::new("config")
                        .short('c')
                        .long("config")
                        .value_name("CONFIG_FILE")
                        .help("TOML config file for scootaloo")
                        .default_value(DEFAULT_CONFIG_PATH)
                        .num_args(1)
                        .display_order(1),
                ),
        )
        .subcommand(
            Command::new("migrate")
            .version(env!("CARGO_PKG_VERSION"))
            .about("Command to migrate Scootaloo DB")
            .arg(
                Arg::new("config")
                    .short('c')
                    .long("config")
                    .value_name("CONFIG_FILE")
                    .help("TOML config file for scootaloo")
                    .default_value(DEFAULT_CONFIG_PATH)
                    .num_args(1)
                    .display_order(1),
            )
            .arg(
                Arg::new("name")
                    .short('n')
                    .long("name")
                    .help("Twitter Screen Name (like https://twitter.com/screen_name, no default)")
                    .num_args(1)
                    .display_order(2)
            )
        )
        .subcommand(
            Command::new("copyprofile")
            .version(env!("CARGO_PKG_VERSION"))
            .about("Command to copy a Twitter profile into Mastodon")
            .arg(
                Arg::new("config")
                    .short('c')
                    .long("config")
                    .value_name("CONFIG_FILE")
                    .help("TOML config file for scootaloo")
                    .default_value(DEFAULT_CONFIG_PATH)
                    .num_args(1)
                    .display_order(1),
            )
            .arg(
                Arg::new("name")
                    .short('n')
                    .long("name")
                    .help("Mastodon Config name (as seen in the config file)")
                    .num_args(1)
                    .display_order(2)
            )
            .arg(
                Arg::new("bot")
                    .short('b')
                    .long("bot")
                    .help("Declare user as bot")
                    .action(ArgAction::SetTrue)
                    .display_order(3)
            )
        )
        .get_matches();

    match matches.subcommand() {
        Some(("register", sub_m)) => {
            register(
                sub_m.get_one::<String>("host").unwrap(),
                sub_m.get_one::<String>("name").unwrap(),
            );
            return;
        }
        Some(("init", sub_m)) => {
            let config = parse_toml(sub_m.get_one::<String>("config").unwrap());
            init_db(&config.scootaloo.db_path).unwrap();
            return;
        }
        Some(("migrate", sub_m)) => {
            let config = parse_toml(sub_m.get_one::<String>("config").unwrap());
            let config_twitter_screen_name =
                &config.mastodon.values().next().unwrap().twitter_screen_name;
            migrate_db(
                &config.scootaloo.db_path,
                sub_m
                    .get_one::<String>("name")
                    .unwrap_or(config_twitter_screen_name),
            )
            .unwrap();
            return;
        }
        Some(("copyprofile", sub_m)) => {
            let mut config = parse_toml(sub_m.get_one::<String>("config").unwrap());
            // filters out the user passed in cli from the global configuration
            if let Some(m) = sub_m.get_one::<String>("name") {
                match config.mastodon.get(m) {
                    Some(_) => {
                        config.mastodon.retain(|k, _| k == m);
                    }
                    None => panic!("Config file does not contain conf for {}", &m),
                }
            }

            profile(config, sub_m.get_flag("bot").then_some(true));

            return;
        }
        _ => (),
    }

    if let Some(level) = matches.get_one::<String>("log_level") {
        SimpleLogger::new()
            .with_level(LevelFilter::from_str(level).unwrap())
            .init()
            .unwrap();
    }

    let config = parse_toml(matches.get_one::<String>("config").unwrap());

    run(config);
}
