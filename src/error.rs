use std::{
    boxed::Box,
    convert::From,
    error::Error,
    fmt::{Display, Formatter, Result},
};

use megalodon::error::Error as megalodonError;

#[derive(Debug)]
pub struct ScootalooError {
    details: String,
}

impl ScootalooError {
    pub fn new(msg: &str) -> ScootalooError {
        ScootalooError {
            details: msg.to_string(),
        }
    }
}

impl Error for ScootalooError {}

impl Display for ScootalooError {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{}", self.details)
    }
}

impl From<Box<dyn Error>> for ScootalooError {
    fn from(error: Box<dyn Error>) -> Self {
        ScootalooError::new(&format!("Error in a subset crate: {error}"))
    }
}

impl From<megalodonError> for ScootalooError {
    fn from(error: megalodonError) -> Self {
        ScootalooError::new(&format!("Error in megalodon crate: {error}"))
    }
}
